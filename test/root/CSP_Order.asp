
<!-- Response.Write("role in CSP_Order.asp1: " & Session("role") & "<BR>") -->


<noscript>
This page requires scripting to be enabled.
</noscript>
<noscript>
This page requires scripting to be enabled.
</noscript>

<HEAD>
<TITLE>SAFER WEB - Company Profile Order</TITLE>
<LINK title="Style Sheet" href="safer.css" rel=stylesheet>
</HEAD>


<!-- Response.Write("header.inc role: " & Session("role") & "<BR>") -->
<!-- Response.Write("path: " & Request.Cookies("COMPASS_PORTAL_PATH").value & "<BR>")-->
<!-- Response.Write("path: " & Request.ServerVariables("ALL_HTTP") & "<BR>") -->

<TABLE cellSpacing=0 cellPadding=1 width="100%" border=0 bordercolor=red summary="Table used for formatting purposes only">

  <TR>
    <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
    <TD vAlign=centered ><IMG alt="FMCSA Logo" SRC="images/SAFER-System.GIF" width="100%" border=0>
</TD>
  </TR>

  <TR>
    <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
    <TD COLSPAN=3><IMG alt="horizontal line" SRC="images/SAFER_hr_half.jpg" width="100%" height="2">
    </TD>
  </TR>

        
</TABLE>


<TABLE>
<TR>
  <TH SCOPE="COL"><div class="hidden">Result Layout</div></TH>
</TR>
<TR>
        <TH SCOPE="ROW"><div class="hidden">Request Form</div></TH>
        <TD>
          <P align="center">
            <FONT size="5" face="arial" color="#2040a0">
              <B><I>Company Safety Profile Request Form &nbsp;</I></B><br>
           </FONT>
          </P>
<!-- BEGIN: SAFER to Interface with Other Systems -->
          <TABLE align=right width=20% border=1 bgcolor=#c0e0ff cellspacing=0 cellpadding=2 summary="For formatting purpose">
          <TR><TH SCOPE="COL"><div class="hidden">Result Layout</div></TH>
          </TR>
          <TR>
            <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
            <TD>
            <TABLE border=0 cellpadding=2 cellspacing=0>
            <TR><TH SCOPE="COL"><div class="hidden">SAFER Table Layout</div></TH>
            </TR>
            <TR>
             <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
             <TD colspan=2 align="center">
             <FONT size="2" face="arial"><B>
              Other Company Safety Profile Resources<br>
             </B></FONT>
             <HR size=2 width=80% color=#0033cc>
             </TD>
            </TR>
            <TR>
             <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
             <TD align=right valign=top><IMG src="Images/bullet_hp.gif" alt="" width=10 height=10></TD>
             <TD align=left><A href="CSP_Help.aspx"><FONT size=2 face=arial>CSP Help</FONT></A></TD>
            </TR><TR>
             <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
             <TD align=right valign=top><IMG src="Images/bullet_hp.gif" alt="" width=10 height=10></TD>
             <TD align=left><A href="http://mcmiscatalog.fmcsa.dot.gov/d_profile_contents.asp"><FONT size=2 face=arial>CSP Definition Document</FONT></A></TD>
            </TR><TR>
             <TH SCOPE="ROW"><div class="hidden">SAFER Table Layout</div></TH>
             <TD align=right valign=top><IMG src="Images/bullet_hp.gif" alt="" width=10 height=10></TD>
             <!--http://infosys.fmcsa.dot.gov/provu.asp-->
	     <TD align=left><A href="http://infosys.fmcsa.dot.gov/PublicSoftware/PublicSoftDoc.aspx?SOFT_NAME=ProVu&NAMEGROUP=ProVu&DOC_TITLE=ProVu"><FONT size=2 face=arial>ProVu (Profile Viewing Application)</FONT></A></TD>
            </TR>
           </TABLE>
          </TABLE>
		  <FONT size="2" face="arial">
           The Company Safety Profile (CSP) contains safety-related information
		   on an individual Company's operation, including selected items from
		   inspection reports and crash reports and the results of any reviews
		   or enforcement actions involving the requested company.  For a
		   more detailed description of the CSP and the information it contains,
		   please select the CSP Definition Document link shown to the right.
		   As an aid to performing a successful CSP Request, please select the
		   CSP Help link shown to the right, particularly if you have not
		   previously used this service.  ProVu is a viewer which allows users
		   to electronically analyze standard company safety profile
		   reports.  To download this software, select the link shown to the
		   right.<br>
		   <br>
                   <HR size=2 width="80%">
		   <H4>Profile Request</H4>
		   <B>To obtain a profile, please enter the information requested below.</B><br>
           <br>
		   <B>Please Note:</B>
		   <UL>
                   
                        <LI>There is a $20.00 fee to request a profile. (The charge will appear as
			    originating from "Computing Technologies, Inc.")<br>

                   


			<LI>Check the Company Snapshot file before requesting a profile to
			    determine how much data FMCSA has on this Company.<br>
			<LI>Profiles will be returned to the user's e-mail address within
			    72 hours.<br>
		
			<LI>Any Company Safety Profile questions should be directed to the
			    FMCSA Information Line at 1-800-832-5660.<br>
		
		    <LI>Enforcement users should use the CSP Request form located on
			    the SAFER Enforcement page.<br>
		
		   </UL>
		   <br>
		   <br>
		   <CENTER><TABLE><TR><TD><FONT size="2" face="arial">
		    <FORM name="CSP_request" ACTION="CSP_Order.asp" METHOD="POST">
             
			 <LABEL for="1"><B>USDOT #:</B></LABEL>
             
<br>
             
             <INPUT id="1" type=text name="USDOT_Number" value="" size=25><br>
             <br>
             
             <LABEL for="2"><B>E-mail address:</B></LABEL><br>
             
             <INPUT id="2" type=text name="EMail" value="" size=25><br>
             <br>
             <INPUT id="3" type="checkbox" Name="DriverInfo" value="DriverInfoRequested"><LABEL for="3">I am the Company whose USDOT number was entered above.</LABEL><br>
			 <br>
			 <CENTER>
			  <input type="submit" value="Submit">
             </CENTER>
			</FORM>
		   </FONT></TD></TR></TABLE></CENTER>
          </FONT>
<!-- END: SAFER to Interface with Other Systems -->

        </TD>
</TR>
</TABLE>
<p align=right>

                 
<table border="0" width="960" align="center" cellpadding="0" cellspacing="0" style="border: solid 0px #e8e8e8;">
  <!-- footer-->
<tr>
    <td colspan="3">
            &#160;
     </td>
</tr>
<tr>
     <td colspan="3" align="center">
          <div id="footerbox">
          <div id="nestedbox" style="background: url(images/logo_footer.gif) no-repeat scroll 20px 40% #FFFFFF;">
         </div>
	<p>
   	<a href="" class="footer_link">SAFER Home</a> <span class="footer_white_text">|</span> 
	
   <a href="http://www.fmcsa.dot.gov/feedback.htm" class="footer_link">Feedback</a>
   <span class="footer_white_text">|</span> 
   <a href="http://www.fmcsa.dot.gov/Online-Privacy-Policy.aspx" class="footer_link">Privacy Policy</a> 
   <span class="footer_white_text">|</span>
   <a href="http://www.usa.gov/" class="footer_link">USA.gov</a> 
   <span class="footer_white_text">|</span> 
   <a href="http://www.fmcsa.dot.gov/foia/foia.htm" class="footer_link">Freedom of Information Act (FOIA)</a> 
   <span class="footer_white_text">|</span> 
   <a href="http://www.fmcsa.dot.gov/508disclaimer.htm" class="footer_link">Accessibility</a> 
   <span class="footer_white_text">|</span>
   <a href="http://www.oig.dot.gov/Hotline" class="footer_link">OIG Hotline</a> 
   <span class="footer_white_text">|</span> 
   <a href="http://www.fmcsa.dot.gov/about/WebPoliciesAndImportantLinks.htm" class="footer_link">Web Policies and Important Links</a> 
   <span class="footer_white_text">|</span> 
   <a href="http://www.fmcsa.dot.gov/plugins.htm" class="footer_link">Plug-ins </a>
   <br />
</p>
<p>
    <span class="footer_title_text">Federal Motor Carrier Safety Administration</span><br />
    <span class="footer_white_text">1200 New Jersey Avenue SE, Washington, DC 20590 &#8226; 1-800-832-5660 &#8226; TTY: 1-800-877-8339 &#8226;</span>
    <a href="http://www.fmcsa.dot.gov/about/contact/offices/displayfieldroster.asp" class="footer_link">Field Office Contacts</a>
</p>
	<p style="margin-bottom: 0em;">&#160;</p>
	</div>
     </td>
</tr>
</table>
