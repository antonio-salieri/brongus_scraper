#!/bin/bash

BULK_SIZE=100000

for ((start=1000000;start > 0; start-=${BULK_SIZE})); do 
    scrapy crawl safer -o ./out/${start}.csv -t csv -a start=${start} -a size=${BULK_SIZE} > ./crawler.log 2>&1;
    sleep 10;
done