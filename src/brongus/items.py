# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BrongusItem(scrapy.Item):
    # define the fields for your item here like:
    link = scrapy.Field()

class CompanyInfoItem(scrapy.Item):
    cargo_carried = scrapy.Field()
    carrier_operation = scrapy.Field()
    operation_clasification = scrapy.Field()
    mcs_150_milage_year = scrapy.Field()
    mcs_150_form_date = scrapy.Field()
    drivers = scrapy.Field()
    power_units = scrapy.Field()
    duns_number = scrapy.Field()
    mc_mx_ff_numbers = scrapy.Field()
    state_carrier_id = scrapy.Field()
    usdot_number = scrapy.Field()
    mailing_address = scrapy.Field()
    phone = scrapy.Field()
    physical_address = scrapy.Field()
    dba_name = scrapy.Field()
    out_of_service_date = scrapy.Field()
    operating_status = scrapy.Field()
    entity_type = scrapy.Field()
    country = scrapy.Field()
    zipcode = scrapy.Field()
