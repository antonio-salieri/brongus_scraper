# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import urljoin
from scrapy.shell import inspect_response
from brongus.items import CompanyInfoItem
import re

def IdGenerator():
    
    for next_url in range(1000000, 0, -1):
        yield next_url

class SaferSpider(scrapy.Spider):
    name = "safer"
    # allowed_domains = ["test.safer.fmcsa.dot.gov"]
    allowed_domains = []
    start_urls = (
        # 'http://test.safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=2796697&original_query_string=BTRANS%20EXPRESS%20LLC',
        'http://127.0.0.1:8125/keywordx.asp?searchstring=%2AILLINOIS%2A&SEARCHTYPE=',
    )
    # start_urls = []

    base_url = "http://safer.fmcsa.dot.gov/query.asp"
    # base_url = "http://127.0.0.1:8125/query.asp"

    def __init__(self, start = 1000000, size = 999999, *args, **kwargs):
        super(SaferSpider, self).__init__(*args, **kwargs)
        self.start = int(start)
        self.size = int(size)
    
    def start_requests(self):
        for id in range(self.start, self.start - self.size, -1):
            print('>>>> %d' % id)
            yield scrapy.FormRequest(self.base_url, 
                                    formdata = {
                                        'searchtype': 'ANY', 
                                        'query_type': 'queryCarrierSnapshot', 
                                        'query_param': 'MC_MX',
                                        'query_string': str(id)},
                                    callback = self.parse_company_info,
                                    dont_filter = True)
    def ystart_requests(self):
        yield scrapy.FormRequest(self.base_url, 
                                formdata = {
                                    'searchtype': 'ANY', 
                                    'query_type': 'queryCarrierSnapshot', 
                                    'query_param': 'MC_MX',
                                    'query_string': '933644'},
                                callback = self.parse_company_info,
                                dont_filter = True)

    def parse(self, response):
        sel = scrapy.Selector(response)
        urls = sel.css('table b > a').xpath("@href").extract()
        
        for url in urls:
            print("Url = %s" % url)
            absolute_url = urljoin(response.url, url)
            yield scrapy.Request(absolute_url, callback=self.parse_company_info)

    def parse_company_info(self, response):
        # sel = scrapy.Selector(response)
        if response.status == 200:
            data_table = response.xpath('//body/table/tr[2]/td/table/tr[2]/td/center[1]/table')
            if data_table:
                company_info = CompanyInfoItem();
                company_info['cargo_carried'] = data_table.xpath('string(tr[19]/td[1])').extract()[0].strip()
                company_info['carrier_operation'] = data_table.xpath('string(tr[16]/td[1])').extract()[0].strip()
                company_info['operation_clasification'] = data_table.xpath('string(tr[14]/td[1])').extract()[0].strip()
                company_info['mcs_150_milage_year'] = data_table.xpath('string(tr[12]/td[2])').extract()[0].strip()
                company_info['mcs_150_form_date'] = data_table.xpath('string(tr[12]/td[1])').extract()[0].strip()
                company_info['drivers'] = data_table.xpath('string(tr[11]/td[2])').extract()[0].strip()
                company_info['power_units'] = data_table.xpath('string(tr[11]/td[1])').extract()[0].strip()
                company_info['duns_number'] = data_table.xpath('string(tr[10]/td[2])').extract()[0].strip()
                company_info['mc_mx_ff_numbers'] = data_table.xpath('string(tr[10]/td[1])').extract()[0].strip()
                print('!!!!! Got MC MX: %s' % company_info['mc_mx_ff_numbers'])
                company_info['state_carrier_id'] = data_table.xpath('string(tr[9]/td[2])').extract()[0].strip()
                company_info['usdot_number'] = data_table.xpath('string(tr[9]/td[1])').extract()[0].strip()
                company_info['mailing_address'] = data_table.xpath('string(tr[8]/td[1])').extract()[0].strip()
                company_info['phone'] = data_table.xpath('string(tr[7]/td[1])').extract()[0].strip()
                address = data_table.xpath('string(tr[6]/td[1])').extract()[0].strip()
                company_info['physical_address'] = address
                
                company_info['country'] = 'N/A'
                m = re.search('[\w\d]+, ([\w]+)[\xa0\s]* \d+', address)
                # inspect_response(response, self)
                if m:
                    company_info['country'] = m.group(1)

                company_info['zipcode'] = 'N/A'
                m = re.search('[\w\d]+, \w+[\xa0\s]* (\d+)', address)
                if m:
                    company_info['zipcode'] = m.group(1)
                
                company_info['dba_name'] = data_table.xpath('string(tr[4]/td[1]/text())').extract()[0].strip()
                company_info['out_of_service_date'] = data_table.xpath('string(tr[3]/td[2]/text())').extract()[0].strip()
                company_info['operating_status'] = data_table.xpath('string(tr[3]/td[1]/text())').extract()[0].strip()
                company_info['entity_type'] = data_table.xpath('string(tr[2]/td[1]/text())').extract()[0].strip()
                return company_info
