# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import re

class StringCleanPipeline(object):
    def process_item(self, item, spider):
        # inspect_response(spider, self)
        for prop in item:
            item[prop] = self.filter_blanks(item[prop])
            item[prop] = self.filter_non_printable(item[prop])
        
        return item

    def filter_blanks(self, str):
        str = re.sub(r'\s+X\s+', ' ', str)
        str = re.sub(r'\r\n|\r|\n', '', str)
        str = re.sub(r'\s+', ' ', str)
        return str

    def filter_non_printable(self, str):
        return ''.join([c for c in str if ord(c) != 34 and ord(c) > 31 and ord(c) < 127])